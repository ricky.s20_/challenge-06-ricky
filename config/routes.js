const express = require("express");
const controllers = require("../app/controllers");
const swaggerUI = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

const apiRouter = express.Router();
apiRouter.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

//login and register
apiRouter.post("/api/v1/user/register", controllers.api.v1.userController.create);
apiRouter.post("/api/v1/user/login", controllers.api.v1.userController.login);

// api with auth user
apiRouter.post("/api/v1/user/create", controllers.api.v1.userController.authorize, controllers.api.v1.userController.create);
apiRouter.get("/api/v1/user", controllers.api.v1.userController.authorize, controllers.api.v1.userController.list);
apiRouter.put("/api/v1/user/:id", controllers.api.v1.userController.authorize, controllers.api.v1.userController.update);
apiRouter.post("/api/v1/user/:id", controllers.api.v1.userController.authorize, controllers.api.v1.userController.updaterole);
apiRouter.get("/api/v1/user/profile", controllers.api.v1.userController.authorize, controllers.api.v1.userController.profile);
apiRouter.get("/api/v1/user/:id", controllers.api.v1.userController.authorize, controllers.api.v1.userController.show);
apiRouter.delete(
  "/api/v1/user/:id",
  controllers.api.v1.userController.authorize, controllers.api.v1.userController.destroy
);

// api with auth
apiRouter.get("/api/v1/cars", controllers.api.v1.userController.authorize, controllers.api.v1.carlistController.list);
apiRouter.get("/api/v1/cars/history", controllers.api.v1.userController.authorize, controllers.api.v1.carlistController.history);
apiRouter.post("/api/v1/cars", controllers.api.v1.userController.authorize, controllers.api.v1.carlistController.create);
apiRouter.put("/api/v1/cars/:id", controllers.api.v1.userController.authorize, controllers.api.v1.carlistController.update);
apiRouter.get("/api/v1/cars/:id", controllers.api.v1.userController.authorize, controllers.api.v1.carlistController.show);
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize, controllers.api.v1.carlistController.destroy
);

apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;